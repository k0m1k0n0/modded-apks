#!/bin/bash

rm *.deb
rm -rf newpack
rm -rf oldpack
rm -rf asarzel

url="https://apt.zel.network/pool/main/z/zelcore/"
wget $(curl $url | rg -e 'href="(.+?\.deb)"' -o -r "${url}"'$1')

mkdir -p newpack oldpack/DEBIAN
dpkg-deb -x *.deb oldpack/

mkdir asarzel
asar extract oldpack/opt/ZelCore/resources/app.asar asarzel


sed -i "s/=_[a-zA-Z0-9]\{1,9\}\['data'\]\['validTill'\]/=1831318200418/g"   asarzel/dist/electron/renderer.js
sed -i "s/=_[a-zA-Z0-9]\{1,9\}\['zelpro'\]\['validTill'\]/=1831318200418/g" asarzel/dist/electron/renderer.js

asar pack asarzel/ oldpack/opt/ZelCore/resources/app.asar


dpkg-deb -e *.deb oldpack/DEBIAN
dpkg-deb -Z xz -b oldpack/ newpack/
